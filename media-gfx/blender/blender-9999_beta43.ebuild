# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
PYTHON_COMPAT=( python3_{6,7} )

inherit git-r3 check-reqs cmake-utils python-single-r1 gnome2-utils xdg-utils pax-utils versionator toolchain-funcs flag-o-matic

DESCRIPTION="3D Creation/Animation/Publishing System"
HOMEPAGE="https://www.blender.org/"

EGIT_REPO_URI="https://git.blender.org/blender.git"
EGIT_BRANCH="master"
EGIT_COMMIT="87aa456ea516f49c6029cf30f8f0dff16cbb58bc"

SLOT="9999"
LICENSE="|| ( GPL-2 BL )"

IUSE="+bullet +dds +compositor +elbeem +game-engine +openexr +sse alembic \
    hdf5 frameserver freestyle +hdr boolean smoke xinput collada colorio \
	cuda cycles debug doc ffmpeg fftw headless jack jemalloc jpeg2k libav \
	llvm man ndof nls openal opencl openimageio openmp opensubdiv openvdb \
	lzo lzma dpx osl player sdl sndfile test tiff valgrind"

REQUIRED_USE="${PYTHON_REQUIRED_USE}
    cuda? ( cycles )
    cycles? ( openexr tiff openimageio )
    opencl? ( cycles )
    osl? ( cycles llvm )
    player? ( game-engine !headless )
"
RDEPEND="${PYTHON_DEPS}
    >=dev-libs/boost-1.62:=[nls?,threads(+)]
    dev-libs/lzo:2
    >=dev-python/numpy-1.10.1[${PYTHON_USEDEP}]
    dev-python/requests[${PYTHON_USEDEP}]
    media-libs/freetype
    media-libs/glew:*
    media-libs/libpng:0=
    media-libs/libsamplerate
    sys-libs/zlib
    virtual/glu
    media-libs/libjpeg-turbo:0=
    virtual/libintl
    virtual/opengl
    collada? ( >=media-libs/opencollada-1.6.18:= )
    colorio? ( media-libs/opencolorio )
    cuda? ( dev-util/nvidia-cuda-toolkit:= )
    ffmpeg? ( media-video/ffmpeg:=[x264,mp3,encode,theora,jpeg2k?] )
    libav? ( >=media-video/libav-11.3:=[x264,mp3,encode,theora,jpeg2k?] )
    fftw? ( sci-libs/fftw:3.0= )
    !headless? ( 
		x11-libs/libX11
		x11-libs/libXi
		x11-libs/libXxf86vm 
    )
    jack? ( virtual/jack )
    jemalloc? ( dev-libs/jemalloc:= )
    jpeg2k? ( media-libs/openjpeg:0 )
    llvm? ( sys-devel/llvm:= )
    ndof? ( app-misc/spacenavd
        dev-libs/libspnav )
    nls? ( virtual/libiconv )
    openal? ( media-libs/openal )
    opencl? ( virtual/opencl )
    openimageio? ( >=media-libs/openimageio-1.7.0 )
    openexr? ( 
		>=media-libs/ilmbase-2.2.0:=
		>=media-libs/openexr-2.2.0:= 
    )
    opensubdiv? ( >=media-libs/opensubdiv-3.3.0:=[cuda=,opencl=] )
    openvdb? ( 
        media-gfx/openvdb[${PYTHON_USEDEP},-abi3-compat(-),abi4-compat(+)]
        dev-cpp/tbb
        >=dev-libs/c-blosc-1.5.2 
    )
    alembic? ( media-gfx/alembic )
    lzma? ( app-arch/lzma )
    lzo? ( dev-libs/lzo )
    osl? ( media-libs/osl:= )
    sdl? ( media-libs/libsdl2[sound,joystick] )
    sndfile? ( media-libs/libsndfile )
    tiff? ( media-libs/tiff:0 )
    valgrind? ( dev-util/valgrind )
"
DEPEND="${RDEPEND}
    >=dev-cpp/eigen-3.2.8:3
    virtual/pkgconfig
    doc? ( 
        app-doc/doxygen[-nodot(-),dot(+),latex]
        dev-python/sphinx[latex] 
    )
    nls? ( sys-devel/gettext )
"

PATCHES=(
	"${FILESDIR}/${PN}-fix-install-rules.patch"
)


blender_check_requirements() {
	[[ ${MERGE_TYPE} != binary ]] && use openmp && tc-check-openmp

	if use doc; then
		CHECKREQS_DISK_BUILD="4G" check-reqs_pkg_pretend
	fi
}


pkg_pretend() {
	blender_check_requirements
}


pkg_setup() {
	blender_check_requirements
	python-single-r1_pkg_setup
}


src_prepare() {
 # Remove some bundled dependencies.
	rm -r \
		extern/glew \
		extern/glew-es \
		extern/Eigen3 \
		extern/lzma \
		extern/lzo \
		extern/gtest \
	|| die

	if use !bullet; then
		rm -r extern/bullet2/*
	fi

	if use !addons; then
		rm -r release/scripts/addons/*
	fi

	if use !addons-contrib; then
		rm -r release/scripts/addons_contrib/*
	fi

	cmake-utils_src_prepare

	# we don't want static glew, but it's scattered across
	# multiple files that differ from version to version
	# !!!CHECK THIS SED ON EVERY VERSION BUMP!!!
	local file
	while IFS="" read -d $'\0' -r file ; do
		sed -i -e '/-DGLEW_STATIC/d' "${file}" || die
	done < <(find . -type f -name "CMakeLists.txt")

	# Disable MS Windows help generation. The variable doesn't do what it
	# it sounds like.
	sed -e "s|GENERATE_HTMLHELP      = YES|GENERATE_HTMLHELP      = NO|" \
	    -i doc/doxygen/Doxyfile || die
}


src_configure() {
	# FIX: forcing '-funsigned-char' fixes an anti-aliasing issue with menu
	# shadows, see bug #276338 for reference
	append-flags -funsigned-char
	append-lfs-flags
	# Blender is compatible ABI 4 or less, so use ABI 4.
	append-cppflags -DOPENVDB_ABI_VERSION_NUMBER=4

	local mycmakeargs=(
		-DPYTHON_INCLUDE_DIR="$(python_get_includedir)"
		-DPYTHON_LIBRARY="$(python_get_library_path)"
		-DPYTHON_VERSION="${EPYTHON/python/}"
		-DWITH_ALEMBIC_HDF5=$(usex hdf5)
		-DWITH_ALEMBIC=$(usex alembic)
		-DWITH_ASSERT_ABORT=$(usex debug)
		-DWITH_BOOST=ON
		-DWITH_BULLET=$(usex bullet)
		-DWITH_C11=ON
		-DWITH_CODEC_FFMPEG=$(usex ffmpeg)
		-DWITH_CODEC_SNDFILE=$(usex sndfile)
		-DWITH_COMPOSITOR=$(usex compositor)
		-DWITH_CUDA=$(usex cuda)
		-DWITH_CXX_GUARDEDALLOC=$(usex debug)
		-DWITH_CXX11=ON
		-DWITH_CYCLES_DEVICE_CUDA=$(usex cuda TRUE FALSE)
		-DWITH_CYCLES_OSL=$(usex osl)
		-DWITH_CYCLES=$(usex cycles)
		-DWITH_DOC_MANPAGE=$(usex man)
		-DWITH_FFTW3=$(usex fftw)
		-DWITH_FRAMESERVER=$(usex frameserver)
		-DWITH_FREESTYLE=$(usex freestyle)
		-DWITH_GAMEENGINE=$(usex game-engine)
		-DWITH_GTESTS=$(usex test)
		-DWITH_HEADLESS=$(usex headless)
		-DWITH_IMAGE_CINEON=$(usex dpx)
		-DWITH_IMAGE_DDS=$(usex dds)
		-DWITH_IMAGE_HDR=$(usex hdr)
		-DWITH_IMAGE_OPENEXR=$(usex openexr)
		-DWITH_IMAGE_OPENJPEG=$(usex jpeg2k)
		-DWITH_IMAGE_TIFF=$(usex tiff)
		-DWITH_INPUT_NDOF=$(usex ndof)
		-DWITH_INSTALL_PORTABLE=OFF
		-DWITH_INTERNATIONAL=$(usex nls)
		-DWITH_JACK=$(usex jack)
		-DWITH_LLVM=$(usex llvm)
		-DWITH_LZMA=$(usex lzma)
		-DWITH_LZO=$(usex lzo)
		-DWITH_MEM_JEMALLOC=$(usex jemalloc)
		-DWITH_MEM_VALGRIND=$(usex valgrind)
		-DWITH_MOD_BOOLEAN=$(usex boolean)
		-DWITH_MOD_FLUID=$(usex elbeem)
		-DWITH_MOD_OCEANSIM=$(usex fftw)
		-DWITH_MOD_REMESH=$(usex remesh)
		-DWITH_MOD_SMOKE=$(usex smoke)
		-DWITH_OPENAL=$(usex openal)
		-DWITH_OPENCL=$(usex opencl)
		-DWITH_OPENCOLLADA=$(usex collada)
		-DWITH_OPENCOLORIO=$(usex colorio)
		-DWITH_OPENIMAGEIO=$(usex openimageio)
		-DWITH_OPENMP=$(usex openmp)
		-DWITH_OPENSUBDIV=$(usex opensubdiv)
		-DWITH_OPENVDB_BLOSC=$(usex openvdb)
		-DWITH_OPENVDB=$(usex openvdb)
		-DWITH_PLAYER=$(usex player)
		-DWITH_PYTHON_INSTALL_NUMPY=OFF
		-DWITH_PYTHON_INSTALL=OFF
		-DWITH_RAYOPTIMIZATION=$(usex sse)
		-DWITH_SDL=$(usex sdl)
		-DWITH_STATIC_LIBS=OFF
		-DWITH_SYSTEM_EIGEN3=ON
		-DWITH_SYSTEM_GLEW=ON
		-DWITH_SYSTEM_LZO=ON
		-DWITH_SYSTEM_OPENJPEG=ON
		-DWITH_X11=$(usex !headless)
		-DWITH_XINPUT=$(usex xinput)
	)
	cmake-utils_src_configure
}


src_compile() {
	cmake-utils_src_compile

	if use doc; then
		# Workaround for binary drivers.
		addpredict /dev/ati
		addpredict /dev/dri
		addpredict /dev/nvidiactl

		einfo "Generating Blender C/C++ API docs ..."
		cd "${CMAKE_USE_DIR}"/doc/doxygen || die
		doxygen -u Doxyfile || die
		doxygen || die "doxygen failed to build API docs."

		cd "${CMAKE_USE_DIR}" || die
		einfo "Generating (BPY) Blender Python API docs ..."
		"${BUILD_DIR}"/bin/blender --background --python doc/python_api/sphinx_doc_gen.py -noaudio || die "sphinx failed."

		cd "${CMAKE_USE_DIR}"/doc/python_api || die
		sphinx-build sphinx-in BPY_API || die "sphinx failed."
	fi
}


src_test() {
	if use test; then
		einfo "Running Blender Unit Tests ..."
		cd "${BUILD_DIR}"/bin/tests || die
		local f
		for f in *_test; do
			./"${f}" || die
		done
	fi
}


src_install() {
	# Pax mark blender for hardened support.
	pax-mark m "${CMAKE_BUILD_DIR}"/bin/blender

	if use doc; then
		docinto "html/API/python"
		dodoc -r "${CMAKE_USE_DIR}"/doc/python_api/BPY_API/.

		docinto "html/API/blender"
		dodoc -r "${CMAKE_USE_DIR}"/doc/doxygen/html/.
	fi

	cmake-utils_src_install

	# fix doc installdir
	docinto "html"
	dodoc "${CMAKE_USE_DIR}"/release/text/readme.html
	rm -r "${ED%/}"/usr/share/doc/blender || die

	python_fix_shebang "${ED%/}/usr/bin/blender-thumbnailer.py"
	python_optimize "${ED%/}/usr/share/blender/${MY_PV}/scripts"
}


pkg_preinst() {
	gnome2_icon_savelist
}


pkg_postinst() {
	gnome2_icon_cache_update
	xdg_mimeinfo_database_update

	elog
	elog "Blender uses python integration. As such, may have some"
	elog "inherit risks with running unknown python scripts."
	elog
	elog "It is recommended to change your blender temp directory"
	elog "from /tmp to /home/user/tmp or another tmp file under your"
	elog "home directory. This can be done by starting blender, then"
	elog "dragging the main menu down do display all paths."
	elog
}


pkg_postrm() {
	gnome2_icon_cache_update
	xdg_mimeinfo_database_update

	ewarn ""
	ewarn "You may want to remove the following directory."
	ewarn "~/.config/${PN}/${MY_PV}/cache/"
	ewarn "It may contain extra render kernels not tracked by portage"
	ewarn ""
}
