# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7
PYTHON_COMPAT=( python3_{5,6,7} )

inherit meson git-r3 xdg-utils

DESCRIPTION="A NVIDIA settings alternative, with fan control, overclocking, and info display."
HOMEPAGE="https://gitlab.com/leinardi/gwe"
EGIT_REPO_URI="${HOMEPAGE}"

SLOT="0"
LICENSE="GPL-3"
KEYWORDS="~amd64 ~x86"

RDEPEND="
	dev-python/pycairo
	dev-python/pygobject
	dev-python/pyinjector
	dev-python/matplotlib
	dev-python/peewee
	dev-python/py3nvml
	dev-python/requests
	dev-python/Rx
	dev-python/pyxdg
	dev-libs/gobject-introspection
	dev-libs/libdazzle
	dev-libs/libappindicator
"
DEPEND="${RDEPEND}
	dev-util/meson
"


pkg_postinst() {
	xdg_desktop_database_update
}
