# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Easily control Intel p-state driver"
HOMEPAGE="https://github.com/pyamsoft/pstate-frequency"
SRC_URI="${HOMEPAGE}/archive/${PV}.tar.gz"

SLOT="0"
LICENSE="MIT"
KEYWORDS="amd64 x86"


src_install() {
	emake PREFIX="/usr" DESTDIR="${D}" install
	einstalldocs
}
