**This repo is being reinstated.**\
*~13th April 2020~*

> **My main project for Gentoo is:** https://framagit.org/3/BIG \
**There will be ebuilds here that center around my own needs that stem from that Gentoo installation guide.\
If you haven't read through it yet, you're bound to learn something new and useful.**

___
<details><summary>Installation</summary>

> **Merging** [app-eselect/eselect-repository](https://packages.gentoo.org/packages/app-eselect/eselect-repository) **is required, layman is not used as it's deprecated.** 

Run these commands with superuser (preferrably with `doas` instead of `sudo`).\
`eselect repository add home git https://framagit.org/3/ebuilds.git`\
`emerge --sync home`\
Every `eix-sync` afterwards will also update this repository in both Portage and Eix.\
`eix-update`

</details>

___

`sys-kernel/gentoo-sources` - https://framagit.org/3/ebuilds/wikis/Agile-Linux \
`x11-terms/xst` - Fork of the st terminal emulator, support for Xresources configuration, and includes other useful patches.\
`app-text/plover` - The free and open source real-time stenography engine.\
`lxqt-base/lxqt-meta` - LXQt meta package with extra USE flags.\
`sys-power/pstate-frequency` - Tool to fine tune Intel CPU frequency with no dependencies.\
`dev-python/pyinjector` - A Python dependency injection framework, inspired by Guice.\
`dev-python/py3nvml` - Python 3 bindings for the NVML library, provides NVIDIA GPU status.\
`www-client/torbrowser-launcher` - Updated with Python 3.7 support, and does not remove AppArmor.
___

**`Git/9999`** - Pulls in the very latest commits from a given branch, usually 'master'; these releases are likely to be less stable.\
`media-gfx/blender` - Blender 2.80.43 beta, using the last commit still including Python 3.6 support, **GCC 7 ONLY**.\
`app-misc/nnn` - Fork of the suckless 'noice' file browser, very fast with vim bindings.\
`sys-power/gwe` - A GTK system utility designed to provide information, control the fans and overclock your NVIDIA video card.\
`sys-apps/firejail` - Firejail ebuild with improved AppArmor installation.\
`games-emulation/pcsx2` - PCSX2 dev ebuild with extra USE flags.\
`media-gfx/flameshot` -  Powerful yet simple to use QT screenshot software.
___
