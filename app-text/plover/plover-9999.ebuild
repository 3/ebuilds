# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7
PYTHON_COMPAT=( python3_{6,7} )

inherit desktop git-r3 python-any-r1

DESCRIPTION="Free and open source real-time stenography engine."
HOMEPAGE="https://www.openstenoproject.org/plover/"
EGIT_REPO_URI="https://github.com/openstenoproject/${PN}.git"

SLOT="0"
LICENSE="GPL-2"

IUSE="debug"

RDEPEND="
    debug? ( dev-python/pytest )
    dev-python/appdirs
    dev-python/Babel
    dev-python/dbus-python
    dev-python/PyQt5
    dev-python/pyserial
    dev-python/python-xlib
    dev-python/setuptools_scm
    dev-python/wcwidth
    dev-python/wxpython
"
DEPEND="${RDEPEND}
"


src_install() {
    python3 setup.py install --root=${D}

    newicon "${PN}/assets/plover.png" "plover.png"
    make_desktop_entry ${PN} Plover plover 'Utility;' ''
}