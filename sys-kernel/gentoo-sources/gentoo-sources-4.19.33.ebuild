# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6
ETYPE="sources"
inherit kernel-2

detect_version
detect_arch

K_DEBLOB_AVAILABLE="1"
K_WANT_GENPATCHES="base extras experimental"
K_GENPATCHES_VER="34"

DESCRIPTION="Agile Linux sources for the ${KV_MAJOR}.${KV_MINOR} kernel tree"
HOMEPAGE="https://framagit.org/3/ebuilds/"

IUSE="+experimental bcachefs iommu pds spectre uksm wireguard"

RDEPEND="app-arch/lz4
"
DEPEND="${RDEPEND}
"

SRC_URI="${KERNEL_URI} ${GENPATCHES_URI} ${ARCH_URI}"
BASEDIR="${FILESDIR}/base/4.19/"
OPTDIR="${FILESDIR}/optional/4.19/"


PATCHES=(
	"${BASEDIR}pci-pme-wakeups.patch"
	"${BASEDIR}intel_idle-tweak-cpuidle-cstates.patch"
	"${BASEDIR}smpboot-reuse-timer-calibration.patch"
	"${BASEDIR}perf.patch"
	"${BASEDIR}glitched-base.patch"
	"${BASEDIR}4.19-bfq-sq-mq-v9r1-2K190204-rc1.patch"
	"${BASEDIR}mqfix.patch"
	"${BASEDIR}i8042-decrease-debug-message-level-to-info.patch"
	"${BASEDIR}silence-rapl.patch"
	"${BASEDIR}Initialize-ata-before-graphics.patch"
	"${BASEDIR}ipv4-tcpbuffer.patch"
	"${BASEDIR}config.patch" 
)


src_prepare() {
	if use bcachefs; then
		eapply "${OPTDIR}bcachefs.patch"
		eapply "${OPTDIR}bcachefs-enable.patch"
	fi

	if use iommu; then
		eapply "${OPTDIR}add-acs-overrides_iommu.patch"
	fi

	if use muqss; then
		eapply "${OPTDIR}v4.19-ck1-full.patch"
		eapply "${OPTDIR}glitched-muqss.patch"
	fi

	if use pds; then
		eapply "${OPTDIR}v4.19_pds099h.patch"
		eapply "${OPTDIR}glitched-pds.patch"
	fi

	if use spectre; then
		eapply "${OPTDIR}retpti-disable.patch"
	fi

	if use uksm; then
		eapply "${OPTDIR}uksm-4.19.patch"
		eapply "${OPTDIR}uksm-enable.patch"
	fi

	if use wireguard; then
		eapply "${OPTDIR}WireGuard-fast-modern-secure-kernel-VPN-tunnel.patch"
	fi

	eapply_user
}
