# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit bash-completion-r1 git-r3 toolchain-funcs

DESCRIPTION="The missing terminal file browser for X"
HOMEPAGE="https://github.com/jarun/nnn"
EGIT_REPO_URI="${HOMEPAGE}"

SLOT="0"
LICENSE="BSD-2"

DEPEND="sys-libs/ncurses:0=
		sys-libs/readline:0=
"
RDEPEND="${DEPEND}
"	

src_prepare() {
	default
	tc-export CC
	sed -i -e '/strip/d' Makefile || die "sed failed"
}


src_install() {
	emake PREFIX="/usr" DESTDIR="${D}" install

	newbashcomp scripts/auto-completion/bash/nnn-completion.bash nnn

	insinto /usr/share/fish/vendor_completions.d
	doins scripts/auto-completion/fish/nnn.fish

	insinto /usr/share/zsh/site-functions
	doins scripts/auto-completion/zsh/_nnn

	einstalldocs
}
